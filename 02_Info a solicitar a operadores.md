# Información a proporcionar por los operadores:

## Servicios fijos de acceso a banda ancha (xDSL, HFC y FTTH):

### Cobertura

Para cada una de lo núcleos de población de Andalucía, porcentaje de **cobertura poblacional** actual y prevista (dentro de un plazo máximo de 3 años) de cada tecnología. Se presentará fichero tipo hoja de cálculo incorporando como mínimo los siguientes campos (se adjunta modelo):

* Código INE 11 dígitos del núcleo de población, según nomenclátor 2013 (se adjunta)
* Denominación del núcleo de población
* Para cada tecnología prestada por el operador en dicha entidad:
	* Porcentaje de cobertura actual del servicio (cobertura poblacional)
	* Número de edificios pasados
	* Número de hogares pasados
	* Porcentaje de cobertura prevista del servicio (cobertura poblacional)
	* Fecha previsión
	* Tasa de transferencia por usuario ofertada en sentido red - usuario

### Nodos de red

* Identificación, localización y caracterización de los **nodos de red** que darán servicio a las entidades de población consideradas. 
Se presentará fichero hoja de cálculo incorporando como mínimo los siguientes campos (se adjunta modelo):
    * Id: código unívoco identificador del nodo de red
	* Código INE del núcleo de población donde se ubica el nodo
	* Tipo: tipología de nodo de red (central telefónica, nodo remoto, etc.)
<!---* Localización: ~~coordenadas x/y UTM ED-50 con un mínimo de 6 decimales--> 
	* Localización: 
		* Sistema de referencia ETRS89. En su defecto, indicar el sistema de coodernadas utilizado.
		* Coordenadas:
			* UTM
			* geográficas (con un mínimo de 4 decimales )
	* Tecnologías ofrecidas desde el nodo: xDSL, VDSlL, HFC y FTTH
	* Conectividad con red troncal mediante fibra óptica: SI / NO

* De manera adicional, se proporcionará **información particular de cobertura** de ciertas zonas que, estando localizadas dentro o fuera de núcleos poblacionales, se consideran de especial interés por parte de la Junta de Andalucía como polígonos industriales, parques naturales, carreteras, etc. Para tal efecto, se proporciona en esta consulta fichero tipo hoja de cálculo con la identificación de dichas zonas y la información solicitada.

###  Trazados de red

* **Trazados de red** con la disposición de la infraestructura troncal (entendida aquella desde los nodos de servicio hasta el core de la red) que formen parte de la prestación de los servicios de acceso a banda ancha, así como el tipo de transmisión de cada enlace. En el caso que por no disponer de la información o estar limitada de alguna forma su distribución por no ser red propia, se proporcionarán los trazados propios y los puntos en los que la red propia interconecta con redes de fibra de otros operadores. 
La información se proporcionará en formato vectorial geo-referenciado soportado por OGR.

###  Planes de extensión

* **Planes de extensión** de nuevas infraestructuras (troncales y de última milla) y servicios en Andalucía durante los próximos 3 años.

## Servicios móviles y fijo inalámbrico (GSM, UMTS, HSPA, LTE, WIMAX):

### Coberturas 


* Datos de cobertura para cada uno de los servicios y tecnologías disponibles, indicando los porcentajes de **cobertura poblacional actual y prevista** (dentro de un plazo máximo de 3 años) para cada una de las entidades de población de Andalucía. Se presentará fichero tipo hoja de cálculo incorporando como mínimo los siguientes campos (se adjunta modelo):
    * Código INE 11 dígitos del núcleos de población, según nomenclátor 2013 (se adjunta)
    * Denominación del núcleo de población
    * Porcentaje de cobertura actual del servicio (cobertura poblacional)
    * Porcentaje de cobertura prevista del servicio (cobertura poblacional)
    * Fecha previsión
    * Tasa de transferencia por usuario ofertada en sentido red - usuario


* **Ráster** **geo-referenciado** **de cobertura actual** para cada servicio. Se presentará un único ráster por servicio y tecnología para toda Andalucía:
	* Resolución mínima de 250 x 250 metros.
	* Indicación de la correspondencia entre el valor del ráster con:
		* Tipo de cobertura: Interior edificios zona urbano, Interior edificios zona Suburbano, Coche, Exterior-Rural, Marginal, Sin cobertura.
		* La magnitud física auditable:  RxLevel (GSM), RSCP (MTS), RSRP(LTE) o similar
		

* Identificación, localización y caracterización de los **nodos de red** que darán servicio a las entidades de población consideradas. Se presentará fichero hoja de cálculo incorporando como mínimo los siguientes campos (se adjunta modelo):
	* Id: código unívoco identificador del nodo de red
	* Localización:
		* Localicación: 
		* Sistema de referencia ETRS89. En su defecto, indicar el sistema de coodernadas utilizado.
		* Coordenadas:
			* UTM
			* Geográficas (con un mínimo de 4 decimales )
    * Tipo: tipología de nodo de red (Estación Base, Nodo B, eNodo B, repetidor, …etc.)
    * Tecnologías ofrecidas desde el nodo (GSM, DCS, UMTS, HSDPA, HSUPA, HSPA+, LMDS, WIMAX, LTE, …etc)
    * Sectores con que cuenta el nodo y azimuth de los mismos
    * Indicación del medio de transmisión con el que se conecta el nodo y capacidad máxima de dicho enlace.
    * Conectividad con red troncal mediante fibra óptica (SI / NO)
* **Trazados de red** en formato vectorial geo-referenciado soportado por OGR, con la disposición de la infraestructura troncal (entendida aquella desde los nodos de servicio hasta el core de la red) que forme parte de la prestación de los servicios móviles o fijo inalámbrico. En el caso que por no disponer de la información o estar limitada de alguna forma su distribución por no ser red propia, se proporcionarán los trazados propios y los puntos en los que la red propia interconecta con redes de fibra de otros operadores. 

* Planes de extensión de nuevas infraestructuras y servicios en Andalucía durante los próximos 3 años. 

De forma general, a la hora de aportar la información sobre nuevas infraestructuras, se incorporará una columna con la fecha estimada de despliegue de la nueva infraestructura en el apartado correspondiente para dicha tipología de infraestructura (puntual o lineal). Si aún se desconoce el detalle concreto de localización de las infraestructuras, se presentará un documento descriptivo de las intenciones de despliegue (zonas preferentes, rangos de población en los que se pretenda actuar, fechas, inversiones previstas…) con el máximo detalle posible.

En el caso de que los planes de despliegue se basen en ampliación de cobertura, se especificarán, en función de la tipología del servicio (cableado o inalámbrico) y tal y como se indica en el apartado correspondiente a dicha tipología del presente documento, indicando los valores finales estimados al finalizar el plan de despliegue previsto e incorporando una columna con la fecha estimada de disponibilidad de la nueva cobertura de servicios, o mediante un nuevo ráster de previsión de cobertura. En el caso en el que se desconozca el detalle a nivel de núcleo, se presentará un documento descriptivo de las intenciones de despliegue (zonas preferentes, rangos de población en los que se pretenda actuar, fechas, inversiones previstas…) con el máximo detalle posible.

## Entidades objetivo del PEBA NGA:

Listado de entidades de población andaluzas que hayan sido incluidos en los proyectos aceptados por el Ministerio de Industria, Energía y Turismo dentro del Programa de Extensión de la Banda Ancha de Nueva Generación en su convocatoria de ayudas de 2013 y 2014. Se presentará fichero tipo hoja de cálculo con al menos los siguientes campos (se adjunta modelo):

 * Código INE 11 dígitos del núcleo, según nomenclátor 2013 (se adjunta)
 * Denominación del núcleo de población
 * Convocatoria Ayuda Minetur (2013 / 2014)
 * Nº Expediente ayuda concedida
 * Título del proyecto que afecta al núcleo de población
 * Tecnología desplegada
 * Porcentaje de cobertura poblacional tras el despliegue

## Otros operadores y entidades

### Trazado de red

* Trazado de red de todo tipo de infraestructuras susceptibles de albergar redes de telecomunicaciones: Capa de red en formato vectorial georreferenciado soportado por OGR, con la disposición de la infraestructura en cuestión e indicación del tipo de la misma (canalización subterránea, tubería, ...etc)  

* Trazado de red de infraestructura troncal: Capa de red en formato vectorial georeferenciado soportado por OGR, con la disposición de la infraestructura troncal (entendida aquella desde los nodos de servicio hasta el core de la red) e indicación del tipo de transmisión de cada enlace (fibra óptica, radioenlace SDH, PDH, etc.). 

### Emplazamientos

* Emplazamientos:
    * Identificación: código unívoco, referencia o similar
    * Localización:
		* Sistema de referencia ETRS89. En su defecto, indicar el sistema de coodernadas utilizado.
		* Coordenadas:
			* UTM
			* geográficas (con un mínimo de 4 decimales )
    * Indicación del tipo de emplazamiento 
    * Tecnologías ofrecidas desde el emplazamiento  
    * Indicación del medio de transmisión con que se conecta el nodo (fibra óptica, radio-enlace SDH, PDH, cable de pares, etc.) y capacidad máxima de dicho enlace.
    
